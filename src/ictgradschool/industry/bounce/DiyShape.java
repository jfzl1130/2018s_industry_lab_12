package ictgradschool.industry.bounce;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DiyShape extends Shape {

    public DiyShape() { super(); }


    public DiyShape(int x, int y, int deltaX, int deltaY){
        super(x,y,deltaX,deltaY);

    }


    public DiyShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);

    }


    @Override
    public void paint(Painter painter) {

        try
        {
            // the line that reads the image file


            Image image = ImageIO.read(new File("main.png"));


            painter.drawImage(image,fX,fY,fWidth,fHeight);

            // work with the image here ...
        }
        catch (IOException e)
        {
            // log the exception
            // re-throw if desired
        }


    }





    }

