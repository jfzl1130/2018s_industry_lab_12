package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {

    public GemShape() {
        super();
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }


    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void paint(Painter painter) {

        if (fWidth > 40) {
            int xpoints[] = {(fX + 20), (fX + fWidth - 20), (fX + fWidth), (fX + fWidth - 20), (fX + 20), (fX)};
            int ypoints[] = {(fY), (fY), (fY + fHeight / 2), (fY + fHeight), (fY + fHeight), (fY + fHeight / 2)};
            int npoints = 6;

            Polygon polygon = new Polygon(xpoints, ypoints, npoints);

            painter.drawPolygon(polygon);

        } else if (fWidth <= 40)  {
                int xpoints[] = {(fX + fWidth / 2), (fX + fWidth), (fX + fWidth / 2), (fX)};
                int ypoints[] = {(fY) , (fY + fHeight / 2) , (fY + fHeight) , (fY + fHeight / 2)};
                int npoints = 4;

                Polygon polygon = new Polygon(xpoints, ypoints, npoints);

                painter.drawPolygon(polygon);

        }
    }
}




