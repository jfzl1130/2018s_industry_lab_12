package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {

    private boolean isFilled;
    private Color colour;


    public DynamicRectangleShape(Color colour) {
        super();
        this.colour = colour;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }


    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        this.colour = Color.black;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
        super(x, y, deltaX, deltaY, width, height);
        this.colour = color;
    }


    @Override
    public void move(int width, int height) {

        int oldfDeltaX = fDeltaX;
        int oldfDeltaY = fDeltaY;


        super.move(width, height);

        if (oldfDeltaX != fDeltaX) {
            isFilled = true;
        } else if (oldfDeltaY != fDeltaY) {
            isFilled = false;
        }

    }


    @Override
    public void paint(Painter painter) {

        if (isFilled) {
            painter.setColor(colour);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.black);
        } else {
            painter.setColor(Color.black);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }


    }
}
